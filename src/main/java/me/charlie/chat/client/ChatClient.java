package me.charlie.chat.client;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.Channel;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioSocketChannel;
import me.charlie.chat.client.network.ChatClientInitializer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

/**
 * Created by Charlie on 17/04/10.
 */
public class ChatClient {
    public static void main(String[] args) throws InterruptedException, IOException {
        System.out.println(ChatColours.GREEN + "What is your name?" + ChatColours.RESET);
        new ChatClient("localhost", 8000).run(new Scanner(System.in).nextLine());
    }

    private final String host;
    private final int port;

    public ChatClient(String host, int port) {
        this.host = host;
        this.port = port;

    }

    public void run(String name) throws InterruptedException, IOException {
        EventLoopGroup group = new NioEventLoopGroup();

        try {
            Bootstrap bootStrap = new Bootstrap()
                    .group(group)
                    .channel(NioSocketChannel.class)
                    .handler(new ChatClientInitializer());

            Channel channel = bootStrap.connect(host, port).sync().channel();
            BufferedReader in = new BufferedReader(new InputStreamReader(System.in));


            channel.writeAndFlush("CONNECT "+ name);
            channel.writeAndFlush("\r\n");

            while (true) {
                channel.writeAndFlush(in.readLine() + "\r\n");
            }
        } finally {
            group.shutdownGracefully();
        }
    }
}
