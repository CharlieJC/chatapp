package me.charlie.chat.server;

/**
 * Created by Charlie on 17/04/10.
 */
public abstract class Command {

    private String command;

    public Command(String command) {
        this.command = command;
    }


    public abstract void execute(User sender, String[] args);
}
