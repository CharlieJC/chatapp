package me.charlie.chat.server;

import java.net.SocketAddress;

/**
 * Created by Charlie on 17/04/10.
 */
public class User {

    private String name;
    private SocketAddress address;
    private boolean isMuted = false;

    public User(String name, SocketAddress address) {
        this.name = name;
        this.address = address;
    }

    public boolean isMuted() {
        return isMuted;
    }

    public void setMuted(boolean muted) {
        isMuted = muted;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SocketAddress getAddress() {
        return address;
    }
}
